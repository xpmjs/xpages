<?php

use \Tuanduimao\Mem as Mem;
use \Tuanduimao\Excp as Excp;
use \Tuanduimao\Err as Err;
use \Tuanduimao\Conf as Conf;
use \Tuanduimao\Model as Model;
use \Tuanduimao\Utils as Utils;

/**
 * 客户检索model
 */

class UserModel extends Model {

	/**
	 * 初始化
	 * @param array $param [description]
	 */
	function __construct( $param=[] ) {
		$param['prefix'] = !empty($param['prefix']) ?  '_baas_' . $param['prefix'] . '_' : '_baas_';
		$param['name'] = !empty($param['name']) ?  $param['name'] : 'user';
		parent::__construct( $param );
		$this->table($param['name']);
	}

	
	/**
	 * 数据表结构
	 * @see https://laravel.com/docs/5.3/migrations#creating-columns
	 * @return [type] [description]
	 */
	function __schema() {
			
		return true;
	}


	function __clear() {
		return true;
	}

}