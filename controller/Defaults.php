<?php
use \Tuanduimao\Loader\App as App;
use \Tuanduimao\Utils as Utils;
use \Tuanduimao\Tuan as Tuan;
use \Tuanduimao\Excp as Excp;
use \Tuanduimao\Conf as Conf;


class DefaultsController extends \Tuanduimao\Loader\Controller {
	
	function __construct() {
	}

	function index() {

		$prefix = !empty($_GET['prefix']) ? trim($_GET['prefix']) : $GLOBALS['_C']['prefix'];
		$table = !empty($_GET['table']) ? trim($_GET['table']) : $GLOBALS['_C']['user.table']; 

		$user = App::M('User',  ['prefix'=>$prefix, 'name'=>$table] );
		$page = !empty($_GET['p']) ? intval($_GET['p']) : 1;

		$rs = $user->query()
					->orderby('created_at', 'desc')
					->paginate(20, ["*"], '', $page )
	    			->toArray();

	    $pages = array();		
		for ($i=1; $i<=$rs["last_page"] ; $i++) { 
			$pages[$i] = $i;
		}

	   	$data = [
	   		'prefix' =>$prefix,
	   		'table'=>$table,
			'page'=>$pages,
			'rs' =>  $rs['data'],
			'cur'=>$rs["current_page"],
			'pre'=>$rs["prev_page_url"],
			'next'=>$rs["next_page_url"],
			'total'=>$rs['total'],
		];


		App::render($data,'defaults','index');
		
		return [
			'js' => [
		 			"js/plugins/select2/select2.full.min.js",
		 			"js/plugins/jquery-validation/jquery.validate.min.js",
		 			"js/plugins/dropzonejs/dropzone.min.js",
		 			"js/plugins/cropper/cropper.min.js",
		 			'js/plugins/masked-inputs/jquery.maskedinput.min.js',
		 			'js/plugins/jquery-tags-input/jquery.tagsinput.min.js',
			 		"js/plugins/dropzonejs/dropzone.min.js",
			 		"js/plugins/cropper/cropper.min.js",
		    		'js/plugins/jquery-ui/jquery-ui.min.js',
	        		'js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js',
				],
			'css'=>[
	 			"js/plugins/select2/select2.min.css",
	 			"js/plugins/select2/select2-bootstrap.min.css"
	 		],
			'crumb' => [
	                 "用户列表" => APP::R('defaults','index'),
	                 "所有用户" =>'',
	        ]
		];
	}


	/**
	 * 转向到用户注册页面
	 * @return [type] [description]
	 */
	function gofront() {
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . App::PR('h5','signup') );
	}

}